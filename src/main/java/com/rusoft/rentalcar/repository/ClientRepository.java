package com.rusoft.rentalcar.repository;

import com.rusoft.rentalcar.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Client getClientByPassport(Integer passport);
}
