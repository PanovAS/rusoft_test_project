package com.rusoft.rentalcar.repository;

import com.rusoft.rentalcar.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {
    Car findCarByBrandAndOwner_Id(String brand, Long ownerId);

    Car findFirstCarByBrandAndYearOfManufactureAndOwnerNull(String brand, Integer yearOfManufacture);
}
