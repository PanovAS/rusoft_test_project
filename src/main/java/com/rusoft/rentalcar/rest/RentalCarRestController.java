package com.rusoft.rentalcar.rest;

import com.rusoft.rentalcar.model.Car;
import com.rusoft.rentalcar.model.Client;
import com.rusoft.rentalcar.service.RentalCarServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rentalcar")
@Api(value = "rentalcar", description = "Operations pertaining with renting a car")
public class RentalCarRestController {
    private final RentalCarServiceImpl rentalCarService;

    @Autowired
    public RentalCarRestController(RentalCarServiceImpl rentalCarService) {
        this.rentalCarService = rentalCarService;
    }

    @ApiOperation(value = "Registers a new client and is looking for a car for him.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Client is registered. Car issued."),
            @ApiResponse(code = 403, message = "The client already has a database."),
            @ApiResponse(code = 404, message = "Free car for a given request is not found.")
    })
    @GetMapping(produces = "application/json")
    public ResponseEntity<String> giveCarToClient(@RequestParam String name,
                                                  @RequestParam Integer passport,
                                                  @RequestParam Integer yearOfBirthday,
                                                  @RequestParam String brand,
                                                  @RequestParam Integer yearOfManufacture) {
        Car car = rentalCarService.findFreeCar(brand, yearOfManufacture);
        if (car != null) {
            if (rentalCarService.isNewClient(passport)) {
                rentalCarService.giveCarToClient(car, name, passport, yearOfBirthday);
                return new ResponseEntity<>("Client is registered. Car issued.", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("The client already has a database.", HttpStatus.FORBIDDEN);
            }
        }
        return new ResponseEntity<>("Free car for a given request is not found.", HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Removes a client from the database. Frees his car.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Client removed. Car released."),
            @ApiResponse(code = 403, message = "The client is not the owner of the specified car."),
            @ApiResponse(code = 404, message = "Client not found in database.")
    })
    @DeleteMapping
    public ResponseEntity<String> takeCarFromClient(@RequestParam("passport") Integer passport,
                                                    @RequestParam("brand") String brand) {
        Client client = rentalCarService.getClient(passport);
        if (client != null) {
            Car car = rentalCarService.getClientCar(brand, client);
            if (car != null) {
                rentalCarService.takeCarFromClient(car, client);
                return new ResponseEntity<>("Client removed. Car released.", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("The client is not the owner of the specified car.", HttpStatus.FORBIDDEN);
            }
        }
        return new ResponseEntity<>("Client not found in database.", HttpStatus.NOT_FOUND);
    }
}
