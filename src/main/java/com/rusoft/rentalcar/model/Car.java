package com.rusoft.rentalcar.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "car")
public class Car extends BaseEntity {
    @Column(nullable = false)
    private String brand;
    @Column(name = "year_of_manufacture", nullable = false)
    private Integer yearOfManufacture;
    @OneToOne
    @JoinColumn(name = "client_id")
    private Client owner;

    public Car() {
    }

    public Car(String brand, Integer yearOfManufacture) {
        this.brand = brand;
        this.yearOfManufacture = yearOfManufacture;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(Integer yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public Client getOwner() {
        return owner;
    }

    public void setOwner(Client owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(brand, car.brand) &&
                Objects.equals(yearOfManufacture, car.yearOfManufacture) &&
                Objects.equals(owner, car.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, yearOfManufacture, owner);
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", yearOfManufacture=" + yearOfManufacture +
                ", owner=" + owner +
                '}';
    }
}
