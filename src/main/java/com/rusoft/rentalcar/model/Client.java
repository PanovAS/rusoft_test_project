package com.rusoft.rentalcar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "client")
public class Client extends BaseEntity {
    @Column(nullable = false)
    private String name;
    @Column(unique = true, nullable = false)
    private Integer passport;
    @Column(name = "year_of_birthday", nullable = false)
    private Integer yearOfBirthday;
    @OneToOne(mappedBy = "owner")
    private Car car;

    public Client() {
    }

    public Client(String name, Integer passport, Integer yearOfBirthday) {
        this.name = name;
        this.passport = passport;
        this.yearOfBirthday = yearOfBirthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPassport() {
        return passport;
    }

    public void setPassport(Integer passport) {
        this.passport = passport;
    }

    public Integer getYearOfBirthday() {
        return yearOfBirthday;
    }

    public void setYearOfBirthday(Integer yearOfBirthday) {
        this.yearOfBirthday = yearOfBirthday;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(name, client.name) &&
                Objects.equals(passport, client.passport) &&
                Objects.equals(yearOfBirthday, client.yearOfBirthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, passport, yearOfBirthday);
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", passport=" + passport +
                ", yearOfBirthday=" + yearOfBirthday +
                '}';
    }
}
