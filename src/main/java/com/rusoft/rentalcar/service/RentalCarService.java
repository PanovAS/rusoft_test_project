package com.rusoft.rentalcar.service;

import com.rusoft.rentalcar.model.Car;
import com.rusoft.rentalcar.model.Client;

public interface RentalCarService {
    Car findFreeCar(String brand, Integer yearOfManufacture);

    boolean isNewClient(Integer passport);

    void giveCarToClient(Car car, String name, Integer passport, Integer yearOfBirthday);

    Client getClient(Integer passport);

    Car getClientCar(String brand, Client client);

    void takeCarFromClient(Car car, Client client);
}
