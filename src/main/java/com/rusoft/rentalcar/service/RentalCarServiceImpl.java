package com.rusoft.rentalcar.service;

import com.rusoft.rentalcar.model.Car;
import com.rusoft.rentalcar.model.Client;
import com.rusoft.rentalcar.repository.CarRepository;
import com.rusoft.rentalcar.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RentalCarServiceImpl implements RentalCarService {
    private final CarRepository carRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public RentalCarServiceImpl(CarRepository carRepository, ClientRepository clientRepository) {
        this.carRepository = carRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public Car findFreeCar(String brand, Integer yearOfManufacture) {
        return carRepository.findFirstCarByBrandAndYearOfManufactureAndOwnerNull(brand, yearOfManufacture);
    }

    @Override
    public boolean isNewClient(Integer passport) {
        return clientRepository.getClientByPassport(passport) == null;
    }

    @Override
    public void giveCarToClient(Car car, String name, Integer passport, Integer yearOfBirthday) {
        Client client = new Client(name, passport, yearOfBirthday);
        clientRepository.save(client);
        car.setOwner(client);
        carRepository.save(car);
    }

    @Override
    public Client getClient(Integer passport) {
        return clientRepository.getClientByPassport(passport);
    }

    @Override
    public Car getClientCar(String brand, Client client) {
        return carRepository.findCarByBrandAndOwner_Id(brand, client.getId());
    }

    @Override
    public void takeCarFromClient(Car car, Client client) {
        car.setOwner(null);
        carRepository.save(car);
        clientRepository.deleteById(client.getId());
    }
}
