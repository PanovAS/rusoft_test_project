package com.rusoft.rentalcar.repository;

import com.rusoft.rentalcar.RentalcarApplication;
import com.rusoft.rentalcar.model.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RentalcarApplication.class)
public class ClientRepositoryTest {
    @Autowired
    private ClientRepository clientRepository;

    @Before
    public void clearTables() {
        clientRepository.deleteAll();
    }

    @Test
    public void getClientByPassport_ClientInDB_ShouldReturnClient() {
        Integer clientPassport = 656565;
        Client expectedClient = clientRepository.save(new Client("Ivan", clientPassport, 1995));
        Client actualClient = clientRepository.getClientByPassport(clientPassport);
        assertEquals(expectedClient, actualClient);
    }

    @Test
    public void getClientByPassport_ClientNotInDB_ShouldReturnNull() {
        Client client = clientRepository.getClientByPassport(656565);
        assertNull(client);
    }
}
