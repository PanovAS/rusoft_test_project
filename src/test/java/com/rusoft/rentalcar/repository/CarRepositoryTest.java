package com.rusoft.rentalcar.repository;

import com.rusoft.rentalcar.RentalcarApplication;
import com.rusoft.rentalcar.model.Car;
import com.rusoft.rentalcar.model.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RentalcarApplication.class)
public class CarRepositoryTest {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Before
    public void clearTables() {
        carRepository.deleteAll();
        clientRepository.deleteAll();
    }

    @Test
    public void findCarByBrandAndOwner_Id_CarInDB_ShouldReturnCar() {
        Client client = clientRepository.save(new Client("Ivan", 645577, 1990));
        Car expectedCar = carRepository.save(new Car("BMW", 2002));
        expectedCar.setOwner(client);
        carRepository.save(expectedCar);
        Car actualCar = carRepository.findCarByBrandAndOwner_Id("BMW", client.getId());
        assertEquals(expectedCar, actualCar);
    }

    @Test
    public void findCarByBrandAndOwner_Id_CarNotInDB_ShouldReturnNull() {
        Client client = clientRepository.save(new Client("Ivan", 645577, 1990));
        Car car = carRepository.findCarByBrandAndOwner_Id("BMW", client.getId());
        assertNull(car);
    }

    @Test
    public void findCarByBrandAndOwner_Id_ClientNotInDB_ShouldReturnNull() {
        carRepository.save(new Car("BMW", 2000));
        Car car = carRepository.findCarByBrandAndOwner_Id("BMW", 1L);
        assertNull(car);
    }

    @Test
    public void findFirstCarByBrandAndYearOfManufactureAndOwnerNull_FreeCarInDB_ShouldReturnFreeCar() {
        carRepository.saveAll(Arrays.asList(new Car("BMW", 2000), new Car("BMW", 2000), new Car("BMW", 2000)));
        Car car = carRepository.findFirstCarByBrandAndYearOfManufactureAndOwnerNull("BMW", 2000);
        assertEquals(new Car("BMW", 2000), car);
        assertNull(car.getOwner());
    }

    @Test
    public void findFirstCarByBrandAndYearOfManufactureAndOwnerNull_FreeCarNotInDB_ShouldReturnNull() {
        carRepository.saveAll(Arrays.asList(new Car("BMW", 2000), new Car("BMW", 2000), new Car("BMW", 2000)));
        Car car = carRepository.findFirstCarByBrandAndYearOfManufactureAndOwnerNull("BMW", 2000);
        assertNotNull(car);
        assertNull(car.getOwner());
    }
}
