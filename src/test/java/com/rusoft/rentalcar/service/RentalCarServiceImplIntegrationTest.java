package com.rusoft.rentalcar.service;


import com.rusoft.rentalcar.RentalcarApplication;
import com.rusoft.rentalcar.model.Car;
import com.rusoft.rentalcar.model.Client;
import com.rusoft.rentalcar.repository.CarRepository;
import com.rusoft.rentalcar.repository.ClientRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RentalcarApplication.class)
public class RentalCarServiceImplIntegrationTest {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private RentalCarService rentalCarService;

    @Before
    public void clearTables() {
        carRepository.deleteAll();
        clientRepository.deleteAll();
    }

    @Test
    public void findFreeCar_FreeCarInDB_ShouldReturnCar() {
        Car expectedCar = carRepository.save(new Car("BMW", 1999));
        Car actualCar = rentalCarService.findFreeCar("BMW", 1999);
        assertEquals(expectedCar, actualCar);
    }

    @Test
    public void findFreeCar_CarNotInDB_ShouldReturnNull() {
        assertNull(rentalCarService.findFreeCar("BMW", 1999));
    }

    @Test
    public void findFreeCar_FreeCarNotInDB_ShouldReturnNull() {
        Client client = clientRepository.save(new Client("Ivan", 656565, 1990));
        Car car = carRepository.save(new Car("BMW", 2002));
        car.setOwner(client);
        carRepository.save(car);
        assertNull(rentalCarService.findFreeCar("BMW", 2002));
    }

    @Test
    public void takeCarFromClient_ShouldDeleteClientAndSetCarOwnerNull() {
        Client client = clientRepository.save(new Client("Ivan", 656565, 1990));
        Car car = carRepository.save(new Car("BMW", 2002));
        car.setOwner(client);
        carRepository.save(car);
        rentalCarService.takeCarFromClient(car, client);
        Car carAfterDeleteClient = carRepository.findFirstCarByBrandAndYearOfManufactureAndOwnerNull("BMW", 2002);
        Client clientAfterDelete = clientRepository.getClientByPassport(656565);
        assertNull(clientAfterDelete);
        assertNull(carAfterDeleteClient.getOwner());
    }

    @Test
    public void getClientCar_CarInDB_ShouldReturnCar() {
        Client client = clientRepository.save(new Client("Ivan", 656565, 1990));
        Car car = carRepository.save(new Car("BMW", 2002));
        car.setOwner(client);
        carRepository.save(car);
        assertEquals(car, rentalCarService.getClientCar("BMW", client));
    }

    @Test
    public void getClientCar_CarNotInDB_ShouldReturnNull() {
        Client client = clientRepository.save(new Client("Ivan", 656565, 1990));
        Car car = carRepository.save(new Car("BMW", 2002));
        car.setOwner(client);
        carRepository.save(car);
        assertNull(rentalCarService.getClientCar("Lada", client));
    }

    @Test
    public void getClient_ClientInDB_ReturnClient() {
        Client client = clientRepository.save(new Client("Ivan", 656565, 1990));
        assertEquals(client, rentalCarService.getClient(656565));
    }

    @Test
    public void getClient_ClientNotInDB_ReturnNull() {
        assertNull(rentalCarService.getClient(656565));
    }

    @Test
    public void giveCarToClient_ShouldSaveClientAndSetCarOwner() {
        Car car = carRepository.save(new Car("BMW", 2002));
        rentalCarService.giveCarToClient(car, "Ivan", 656565, 1990);
        Client newClient = clientRepository.getClientByPassport(656565);
        Car givenCar = carRepository.findCarByBrandAndOwner_Id("BMW", newClient.getId());
        assertNotNull(newClient);
        assertEquals(newClient, givenCar.getOwner());
    }

    @Test
    public void isNewClient_ClientInDB_ShouldFalse() {
        clientRepository.save(new Client("Ivan", 656565, 1990));
        assertFalse(rentalCarService.isNewClient(656565));
    }

    @Test
    public void isNewClient_ClientNotInDB_ShouldTrue() {
        assertTrue(rentalCarService.isNewClient(656565));
    }
}
