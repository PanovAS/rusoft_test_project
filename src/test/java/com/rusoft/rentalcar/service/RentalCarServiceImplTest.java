package com.rusoft.rentalcar.service;

import com.rusoft.rentalcar.model.Car;
import com.rusoft.rentalcar.model.Client;
import com.rusoft.rentalcar.repository.CarRepository;
import com.rusoft.rentalcar.repository.ClientRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RentalCarServiceImplTest {
    @Mock
    private Client client;
    @Mock
    private Car car;
    @Mock
    private CarRepository carRepository;
    @Mock
    private ClientRepository clientRepository;
    @InjectMocks
    private RentalCarServiceImpl rentalCarService;

    @Test
    public void findFreeCar_FreeCarInDB_ShouldReturnCar() {
        when(carRepository.findFirstCarByBrandAndYearOfManufactureAndOwnerNull(anyString(), anyInt())).thenReturn(car);
        assertEquals(car, rentalCarService.findFreeCar(anyString(), anyInt()));
        verify(carRepository, times(1)).findFirstCarByBrandAndYearOfManufactureAndOwnerNull(anyString(), anyInt());
    }

    @Test
    public void findFreeCar_FreeCarNotInDB_ShouldReturnNull() {
        when(carRepository.findFirstCarByBrandAndYearOfManufactureAndOwnerNull(anyString(), anyInt())).thenReturn(null);
        assertNull(rentalCarService.findFreeCar(anyString(), anyInt()));
        verify(carRepository, times(1)).findFirstCarByBrandAndYearOfManufactureAndOwnerNull(anyString(), anyInt());
    }

    @Test
    public void takeCarFromClient_ShouldDeleteClientAndSetCarOwnerNull() {
        rentalCarService.takeCarFromClient(car, client);
        verify(car, times(1)).setOwner(null);
        verify(carRepository, times(1)).save(car);
        verify(client, times(1)).getId();
        verify(clientRepository, times(1)).deleteById(anyLong());
    }

    @Test
    public void getClientCar_CarInDB_ShouldReturnCar() {
        when(client.getId()).thenReturn(1L);
        when(carRepository.findCarByBrandAndOwner_Id("BMW", 1L)).thenReturn(car);
        assertEquals(car, rentalCarService.getClientCar("BMW", client));
        verify(carRepository, times(1)).findCarByBrandAndOwner_Id(anyString(), anyLong());
        verify(client, times(1)).getId();
    }

    @Test
    public void getClientCar_CarNotInDB_ShouldReturnNull() {
        when(client.getId()).thenReturn(1L);
        when(carRepository.findCarByBrandAndOwner_Id("BMW", 1L)).thenReturn(null);
        assertNull(rentalCarService.getClientCar("BMW", client));
        verify(carRepository, times(1)).findCarByBrandAndOwner_Id(anyString(), anyLong());
        verify(client, times(1)).getId();
    }

    @Test
    public void getClient_ClientInDB_ReturnClient() {
        when(clientRepository.getClientByPassport(anyInt())).thenReturn(client);
        assertEquals(client, rentalCarService.getClient(anyInt()));
        verify(clientRepository, times(1)).getClientByPassport(anyInt());
    }

    @Test
    public void getClient_ClientNotInDB_ReturnNull() {
        when(clientRepository.getClientByPassport(anyInt())).thenReturn(null);
        assertNull(rentalCarService.getClient(anyInt()));
        verify(clientRepository, times(1)).getClientByPassport(anyInt());
    }

    @Test
    public void giveCarToClient_ShouldSaveClientAndSetCarOwner() {
        String clientName = "Ivan";
        Integer clientPassport = 656565;
        Integer clientYearOfBirthday = 1990;
        rentalCarService.giveCarToClient(car, clientName, clientPassport, clientYearOfBirthday);
        Client newClient = new Client(clientName, clientPassport, clientYearOfBirthday);
        verify(clientRepository, times(1)).save(newClient);
        verify(car, times(1)).setOwner(newClient);
        verify(carRepository, times(1)).save(car);
    }

    @Test
    public void isNewClient_ClientInDB_ShouldFalse() {
        when(clientRepository.getClientByPassport(anyInt())).thenReturn(client);
        assertFalse(rentalCarService.isNewClient(anyInt()));
        verify(clientRepository, times(1)).getClientByPassport(anyInt());
    }

    @Test
    public void isNewClient_ClientNotInDB_ShouldTrue() {
        when(clientRepository.getClientByPassport(anyInt())).thenReturn(null);
        assertTrue(rentalCarService.isNewClient(anyInt()));
        verify(clientRepository, times(1)).getClientByPassport(anyInt());
    }
}
